from django.db import models
from django.contrib.auth.models import User
from model_utils.models import TimeStampedModel

# Create your models here.

class Product(TimeStampedModel):
    """
    商品
    TimeStampedModel:
        created: 建立時間
        modified: 修改時間
    """

    class SpecSize(models.TextChoices):
        XL = "xl", "xl"
        L = "l", "l"
        SM = "sm", "sm"

    # gender = models.CharField("性別", max_length=16, choices=Gender.choices, blank=True, null=True)
    name = models.CharField("商品名稱", max_length=256)
    price = models.IntegerField("商品價格")
    thumb_img = models.ImageField("商品圖片", upload_to="product/thumb_img", blank=True, null=True)
    spec_size = models.CharField("尺寸", max_length=4, choices=SpecSize.choices)
    category = models.ManyToManyField("product.ProductCategory", verbose_name="商品類別", blank=True)

    def __str__(self):
        return self.name

class ProductImage(TimeStampedModel):
    """
    商品圖片
    TimeStampedModel:
        created: 建立時間
        modified: 修改時間
    """

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    img = models.ImageField("商品圖片", upload_to="product/product_img", blank=True, null=True)
    def __str__(self):
        return self.product.name

class ProductCategory(models.Model):
    name = models.CharField("商品類別", max_length=256)
    def __str__(self):
        return self.name
