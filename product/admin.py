from django.contrib import admin
from product.models import Product, ProductImage, ProductCategory

class ProductImageInline(admin.StackedInline):
    model = ProductImage
    verbose_name = "產品圖片"
    verbose_name_plural = "產品圖片"

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    inlines = [
        ProductImageInline,
    ]

    list_display = (
        "name",
        "price",
        "spec_size",
    )
    # raw_id_fields = ("category",)
    filter_horizontal = ("category",)

    def view_name(self, obj: Product):
        return obj.name


@admin.register(ProductImage)
class ProductImageAdmin(admin.ModelAdmin):
    list_display = (
        "product",
    )
    raw_id_fields = ("product",)

    def view_name(self, obj: Product):
        return obj.product.name

@admin.register(ProductCategory)
class ProductCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
    )
    raw_id_fields = ("product",)

    def view_name(self, obj: ProductCategory):
        return obj.name
