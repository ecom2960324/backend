import os

# Typing
from argparse import ArgumentParser
from getpass import getpass
from typing import Any

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from datetime import datetime
import argparse
from data.city_list import CITY_LIST

def date_type(date_string):
    try:
        return datetime.strptime(date_string, "%Y-%m-%d")
    except ValueError:
        msg = "日期型態不正確 YYYY-MM-DD: '{0}'.".format(date_string)
        raise argparse.ArgumentTypeError(msg)

class Command(BaseCommand):
    help = "Initialize procedures"

    def add_arguments(self, parser: ArgumentParser) -> None:
        parser.add_argument("-s", "--str", type=date_type, required=False, help="force import")
        parser.add_argument("-f", "--force", action="store_true", help="force import")
        parser.add_argument("-d", "--demo", action="store_true", help="demo data")

    def handle(self, *args: Any, **options: Any) -> None:
        # 建立使用者

        # 建立商品

        # 建立供應商

        # 建立縣市清單
        # print(CITY_LIST)
        for city, town in CITY_LIST.items():
            print(city)
            print(town)
            break

        # 取得使用者

        # 建立訂單

    def create_user(self) -> None:
        return
