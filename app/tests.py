from django.test import TestCase
from app.models import Profile
from django.contrib.auth.models import User


class ProfileTestCase(TestCase):
    def setUp(self):
        lion = User.objects.create_user(username="lion", password="123456")
        cat = User.objects.create_user(username="cat", password="123456")
        lion_pro = Profile.objects.create(user=lion)
        cat_pro = Profile.objects.create(user=cat)
        aa = {"gender": "male"}
        Profile.objects.filter(user__username="lion").update(**aa)
        # Profile
        cat_pro.gender = "female"
        cat_pro.save()

    def test_animals_can_speak(self):
        """Animals that can speak are correctly identified"""
        male = Profile.objects.filter(user__profile__gender="male").first()
        female = Profile.objects.filter(user__profile__gender="female").first()
        self.assertEqual(male.user.username, 'lion')
        self.assertEqual(female.user.username, 'cat')
