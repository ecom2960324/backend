from django.contrib import admin

from app.models import Profile
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

# Register your models here.

# 先卸載 User 的 admin site
admin.site.unregister(User)


# 重新註冊 User 的 admin site
class ProfileInline(admin.StackedInline):
    model = Profile
    verbose_name = "個人資料"
    verbose_name_plural = "個人資料1"


class UserInlineAdmin(UserAdmin):
    inlines = [
        ProfileInline,
    ]


admin.site.register(User, UserInlineAdmin)

# 註冊 Profile 的 admin site
@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = (
        "user",
        "gender",
    )
    raw_id_fields = ("user",)

    def view_name(self, obj: Profile):
        return obj.user.first_name
