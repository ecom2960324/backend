from functools import wraps

# Reference:
# https://chase-seibert.github.io/blog/2013/12/17/python-decorator-optional-parameter.html
# https://django-role-permissions.readthedocs.io/en/stable/

class GetPermission:
    def __init__(self, method_permission: dict = {}):
        self.method_permission = method_permission

    def __call__(self, func):
        # The `@wraps(func)` decorator is used to preserve the metadata of the original function `func` when creating a new function `callable`.
        @wraps(func)
        def callable(obj, *args, **kwargs):
            if obj.request.method == "GET":
                obj.permission_classes = self.method_permission.get("GET", obj.permission_classes)
            elif obj.request.method == "POST":
                obj.permission_classes = self.method_permission.get("POST", obj.permission_classes)
            elif obj.request.method == "PUT":
                obj.permission_classes = self.method_permission.get("PUT", obj.permission_classes)
            elif obj.request.method == "PATCH":
                obj.permission_classes = self.method_permission.get("PATCH", obj.permission_classes)
            elif obj.request.method == "DELETE":
                obj.permission_classes = self.method_permission.get("DELETE", obj.permission_classes)
            elif obj.request.method == "OPTIONS":
                obj.permission_classes = self.method_permission.get("OPTIONS", obj.permission_classes)

            return func(obj, *args, **kwargs)
        return callable
