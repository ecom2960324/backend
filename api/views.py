import logging
from typing import Any

# from dj_rest_auth.jwt_auth import JWTCookieAuthentication
from rest_framework_simplejwt.authentication import JWTAuthentication
from django.contrib.auth import get_user_model
from django.http import HttpRequest
from drf_spectacular.utils import OpenApiResponse, extend_schema

# https://www.django-rest-framework.org/api-guide/status-codes/
from rest_framework import status as Status_code
from rest_framework.generics import GenericAPIView
from django.utils.decorators import method_decorator

# from rest_framework.parsers import FormParser, JSONParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from api.decorators.get_permission import GetPermission

logger = logging.getLogger(__name__)


class UserDataApiView(GenericAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    @GetPermission(method_permission={"GET": [IsAuthenticated], "POST": [IsAuthenticated]})
    def get_permissions(self):
        """
        Override this method to customize permissions for different methods.
        """
        return super().get_permissions()


    @extend_schema(
        tags=["System"],
        summary="取得使用者資訊",
        description="[JWT] [GET] /api/user-data/",
        responses={200: str, 401: str},
    )
    def get(self, request: HttpRequest, *args: Any, **kw: Any) -> Response:
        User = get_user_model()
        current_user = User.objects.filter(id=request.user.id).first()
        return Response({})

    @extend_schema(
        tags=["System"],
        summary="取得使用者資訊",
        description="[JWT] [POST] /api/user-data/",
        responses={200: str, 401: str},
    )
    def post(self, request: HttpRequest, *args: Any, **kw: Any) -> Response:
        User = get_user_model()
        current_user = User.objects.filter(id=request.user.id).first()
        return Response({})

