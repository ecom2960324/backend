include .env

PYVENV_PREFIX=pipenv run

format:
	$(PYVENV_PREFIX) black .
	$(PYVENV_PREFIX) isort .

lint:
	$(PYVENV_PREFIX) flake8 .

shell:
	$(PYVENV_PREFIX) python manage.py shell_plus --print-sql

run:
	$(PYVENV_PREFIX) python manage.py runserver 0.0.0.0:3005

migrate:
	$(PYVENV_PREFIX) python manage.py migrate

makemigrations:
	$(PYVENV_PREFIX) python manage.py makemigrations

collectstatic:
	$(PYVENV_PREFIX) python manage.py collectstatic

init:
	$(PYVENV_PREFIX) python manage.py initialize -f

rm-migrations:
	find . -path '*/migrations/__init__.py' -exec truncate -s 0 {} + -o -path '*/migrations/*' -delete

# Schema 關係圖
erd:
	$(shell $(PYVENV_PREFIX) python manage.py graph_models -a > ./doc/erd/erd.dot && $(PYVENV_PREFIX) python manage.py graph_models --pydot -a -g -o ./doc/erd/erd.png)
